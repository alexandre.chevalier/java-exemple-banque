/**
 * Classe principale du programme.<br>
 * Sert uniquement à démarrer le programme via la méthode
 * {@link #main(String[])}.
 */
public class Programme {

	public static void main(String[] args) {
		// Initialisation du service bancaire
		ServiceBancaire serviceBancaire = new ServiceBancaire();
		// Chargement des données (ici depuis un fichier)
		// Mais on peut imaginer que cette méthode fait une requête en base de données
		serviceBancaire.chargerComptesBancaires();

		// Cette section pour être réalisée par un service de gestion des utilisateurs
		// Par exemple pour s'assurer de l'authentification des utilisateurs
		// Mais ici on les instancie directement pour l'exemple
		Utilisateur user1 = new Utilisateur("ID05", "Alexandre", "Chevalier");
		Utilisateur user2 = new Utilisateur("ID08", "Nom", "Prénom");

		CompteBancaire compte1;
		CompteBancaire compte2;

		try {
			compte1 = serviceBancaire.recupererCompte(user1);
			compte2 = serviceBancaire.recupererCompte(user2);

			compte1.afficherInfos();
			compte2.afficherInfos();

			// TODO : on peut alors effectuer des opérations bancaires ici...

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
