/**
 * Modélisation d'un compte bancaire.
 */
public class CompteBancaire {

	// Attributs
	private String identifiant;
	private float solde;
	private float decouvert;

	// Constructeur principal qui remplace celui par défaut
	public CompteBancaire(String identifiant, float solde, float decouvert) {
		super();
		this.identifiant = identifiant;
		this.solde = solde;
		this.decouvert = decouvert;
	}

	/**
	 * Ajouter de l'argent au compte.
	 * 
	 * @param montant La somme à ajouter
	 */
	public void ajouter(float montant) {
		solde += montant;
	}

	/**
	 * Retirer de l'argent au compte.
	 * <p>
	 * Effectue une vérification du montant avant retrait.
	 * 
	 * @param montant La somme à retirer
	 */
	public void retirer(float montant) {
		// Si montant insuffisant je sors de la fonction (return)
		if (montant > solde + decouvert) {
			System.out.println("Solde insuffisant !");
			return;

		} else {
			solde -= montant;
		}
	}

	/**
	 * Affiche les informations du compte.
	 */
	public void afficherInfos() {
		String s = String.format("Identifiant : %s \nSolde : %.2f  \nDécouvert autorisé : %.2f ", identifiant, solde,
				decouvert);
		System.out.println("----------------------------");
		System.out.println(s);
		System.out.println("----------------------------");
	}

	// ------------------
	// Getters et setters
	// ------------------
	
	public String getIdentifiant() {
		return identifiant;
	}

	public float getSolde() {
		return solde;
	}

}
