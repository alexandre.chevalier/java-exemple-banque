/**
 * Modélisation d'un Utilisateur pour notre application.
 */
public class Utilisateur {

	// Attributs / propriétés
	private String numeroCompte;
	private String nom;
	private String prenom;

	// Constructeur principal (remplace celui par défaut)
	public Utilisateur(String numeroCompte, String nom, String prenom) {
		super();
		this.numeroCompte = numeroCompte;
		this.setNom(nom);
		this.setPrenom(prenom);
	}

	// ------------------
	// Getters et setters
	// ------------------
	
	public String getNumeroCompte() {
		return numeroCompte;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
