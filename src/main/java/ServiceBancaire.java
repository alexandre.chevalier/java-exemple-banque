import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Service permettant de :
 * 
 * <li>Charger des comptes bancaires
 * <li>Renvoyer la liste des comptes
 * <li>Récupérer un compte par son identifiant
 */
public class ServiceBancaire {

	/** La liste des comptes bancaires qui sera chargée en mémoire. */
	final static List<CompteBancaire> COMPTES_BANCAIRES = new ArrayList<CompteBancaire>();

	/**
	 * Charge les comptes bancaires dans une liste.
	 */
	public void chargerComptesBancaires() {
		System.out.println("Chargement des comptes...");

		List<String> lignesDuFichier = new ArrayList<String>();
		try {
			// On charge toutes les lignes du fichier dans une liste
			// Attention ! Cette opération est coûteuse en RAM quand le fichier est gros
			lignesDuFichier = Files.readAllLines(new File("src/comptes.csv").toPath());

			for (int i = 0; i < lignesDuFichier.size(); i++) {
				String ligne = lignesDuFichier.get(i);
				// Chaque ligne est de la forme "xxx;xxx;xxx"
				// alors on la découpe en 3 éléments
				String[] ligneDecoupee = ligne.split(";");

				String identifiant = ligneDecoupee[0];
				String solde = ligneDecoupee[1];
				String decouvert = ligneDecoupee[2];

				// Conversion des String en float
				float soldeFlottant = Float.parseFloat(solde);
				float decouvertFlottant = Float.parseFloat(decouvert);

				// Instancier les comptes et les ajouter à la liste
				CompteBancaire compte = new CompteBancaire(identifiant, soldeFlottant, decouvertFlottant);
				COMPTES_BANCAIRES.add(compte);
			}
			System.out.println("Les comptes ont été chargés.");
		} catch (IOException e) {
			// En cas d'erreur on affiche un message puis le détail de l'erreur si besoin
			System.err.println("Impossible de lire le fichier !");
			System.err.println("Détails de l'erreur :");
			e.printStackTrace();
		}
	}

	/**
	 * Renvoie la liste des comptes bancaires.<br>
	 * <b>Attention ! Doit être exécutée après le chargement des comptes.</b>
	 * 
	 * @return La listes des comptes
	 */
	public List<CompteBancaire> listeDesComptes() {
		return COMPTES_BANCAIRES;
	}

	/**
	 * Récupère le compte d'un client.
	 * 
	 * @param user Un client
	 * @return Le {@link CompteBancaire} correspondant à l'{@link Utilisateur}
	 * @throws Exception si aucun compte n'a été trouvé
	 */
	public CompteBancaire recupererCompte(Utilisateur user) throws Exception {
		String id = user.getNumeroCompte();
		if (id != null) {
			for (int i = 0; i < COMPTES_BANCAIRES.size(); i++) {
				CompteBancaire compteBancaire = COMPTES_BANCAIRES.get(i);
				String identifiant = compteBancaire.getIdentifiant();

				if (id.equals(identifiant)) {
					return compteBancaire;
				}
			}
		}
		// Si aucun compte n'a été trouvé on lance une exception
		throw new Exception("Compte bancaire introuvable pour utilisateur :" + user.getNumeroCompte());
	}
}
